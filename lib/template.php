<?php

function render($template, $vars=array()) {
    $content = '';
    if (stream_resolve_include_path($template . '.phtml')) {
        ob_start();
        if (is_array($vars)) {
          extract($vars);  
        }
        
        require($template . '.phtml');
        $content = ob_get_clean();
    } else {
        error_log ('Error: ' . $template . ' not found!');
    }

    return $content;
}


