  <?php

// Include our necessary files
require_once 'conf/env.php'; // Sets include path
require_once 'lib/template.php'; // Brings in our template function


/* This next bit of code pulls our controller path and action path from the
 * request URI.  If it is blank, it makes some default assignments.
 */

// Get rid of the query string if it's there
$path = array_shift(explode('?', $_SERVER['REQUEST_URI']));

// Now get rid of the Base directory
$path = preg_replace('|^' . APP_BASE . '|', '', $path);
$path_vars = explode('/', $path);

// Define our controller and action
$controller = array_shift($path_vars);
$action = array_shift($path_vars);

// Get any url key/values and assign them to the _GET array
for ($x=0; $x<count($path_vars); $x++) {
  $_GET[$path_vars[$x]] = $path_vars[++$x];
}

// Make sure we have a good controller/action path
if ('' != $controller && '' == $action) { // If controller is empty but action is not, they just sent us the controller
  $controller = $action;
  $action = 'index';
} elseif ('' == $controller && '' == $action) { // If they're both empty, assign defaults
  $controller = 'default';
  $action = 'index';
}

// Default parameter instantiation
$content = '';      // Variable controller will assign content into
$params = array();  // Array controller should assign values to
$template = '';     // Default template if controller does not assign one

// Validate the controller they requested exists, if not, 404
if (stream_resolve_include_path("controllers/{$controller}/{$action}.php") === false) {
  $controller = 'default';
  $action = '404';
}

// Load up the requested controller
include "controllers/{$controller}/{$action}.php";

// Render content from action. Tempalte and params set in action script
$params['content'] = render($template, $params);

if (isset($render_layout) && $render_layout === false) {
  $finalContent = $params['content'];
} else {
  // If no title is provided, from controller, create default title
  if(empty($params['title'])) {
    $params['title'] = $_SERVER['HTTP_HOST'];
  }
  
  // Render final content utilizing variable content from controller script...
  $finalContent = render('templates/layout', $params);
}

// Output rendered content
echo $finalContent;
