<?php

set_include_path(get_include_path() . PATH_SEPARATOR . 
                 dirname(dirname(__FILE__)) . '/templates/' . PATH_SEPARATOR .
                 dirname(dirname(__FILE__)));

define('APP_BASE', '/');
date_default_timezone_set('America/Chicago');